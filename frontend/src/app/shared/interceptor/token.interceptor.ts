import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../service/auth.service';
import {NotificationService} from '../service/notification.service';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService,
                private router: Router,
                private notificationService: NotificationService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: this.authService.isLoggedIn() ? {
                Authorization: `Bearer ${this.authService.getToken()}`,
                'Content-Type': 'application/json;charset=UTF-8',
                Accept: 'application/json, text/plain, */*'
            } : {
                'Content-Type': 'application/json;charset=UTF-8',
                Accept: 'application/json, text/plain, */*'
            }
        });
        return next.handle(request)
            .pipe(catchError((err) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401 || err.status === 403) {
                        this.authService.logout();
                        this.router.navigateByUrl(`/login?redirectUrl=${this.router.url}`);
                        this.notificationService.showNotification('warning', '', 'Forbidden!');
                    } else if (err.status === 500) {
                        this.notificationService.showNotification('danger', '', 'an error has occured!');
                    }
                }
                return of(HttpErrorResponse);
            }))as Observable<HttpEvent<any>>;
    }
}
