import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {GenericService} from './generic.service';
import {Injectable} from '@angular/core';
import {Config} from '../config/config';
import {StorageService} from './storage.service';
import {Credentials} from '../model/credentials';
import {Admin} from '../model/admin';
import {Observable} from 'rxjs/Observable';
import {LoginResponse} from '../../layouts/login-layout/login/model/response/login.response';


@Injectable()
export class AuthService extends GenericService {
    loggedAdmin: Admin = new Admin();

    constructor(private http: HttpClient, private storageService: StorageService) {
        super();
    }


    login(credentials: Credentials): Observable<LoginResponse> {
        const url = Config.baseUrl + '/user/guest/login';
        return this.http.post<any>(url, credentials)
            .pipe(catchError(this.handleErrors)) as Observable<LoginResponse>;
    }

    refreshAuthUser(): Observable<Admin> {
        const url = Config.baseUrl + '/user/me';
        return this.http.get<any>(url)
            .pipe(catchError(this.handleErrors))
            .pipe(map((data: Admin) => {
                this.loggedAdmin = data;
                return data;
            })) as Observable<Admin>;
    }

    isLoggedIn() {
        return this.storageService.read('admin-token') != null;
    }

    logout() {
        this.storageService.removeAll();
    }

    getToken() {
        return this.storageService.read('admin-token');
    }
}
