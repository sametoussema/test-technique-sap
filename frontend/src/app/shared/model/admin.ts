export class Admin {
    id: string;
    username: string;
    active: boolean;
    authorities: Array<string>;
}
