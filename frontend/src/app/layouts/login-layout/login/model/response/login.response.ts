import {Admin} from '../../../../../shared/model/admin';

export class LoginResponse {
    accessToken: string;
    user: Admin;
}
