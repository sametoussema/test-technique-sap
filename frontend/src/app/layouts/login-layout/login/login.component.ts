import {Component, OnInit} from '@angular/core';
import {Credentials} from '../../../shared/model/credentials';
import {AuthService} from '../../../shared/service/auth.service';
import {StorageService} from '../../../shared/service/storage.service';
import {Router} from '@angular/router';
import {LoginResponse} from './model/response/login.response';
import {Subscription} from 'rxjs/Subscription';
import {NotificationService} from '../../../shared/service/notification.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    credentials: Credentials = new Credentials();
    busy: Subscription;

    constructor(private authService: AuthService,
                private storageService: StorageService,
                private router: Router,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
    }

    login() {
        this.credentials.password = this.credentials.password.split(' ').join('');
        this.busy = this.authService.login(this.credentials)
            .subscribe(
                (data: LoginResponse) => {
                    this.storageService.write('admin', data.user);
                    this.authService.loggedAdmin = data.user;
                    this.storageService.write('admin-token', data.accessToken);
                    this.notificationService.showNotification('success', '', 'Welcome <br>' + this.credentials.username + '</br>');
                    this.router.navigate(['/dashboard'], {queryParams: {reload: true}});
                },
                (error) => {
                    console.log(error);
                    this.notificationService.showNotification('danger', '', 'Incorrect username or password.');
                }
            )
    }

}
