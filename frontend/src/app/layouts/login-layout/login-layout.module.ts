import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {MatButtonModule, MatInputModule, MatRippleModule, MatTooltipModule} from '@angular/material';
import {LoginLayoutRoutes} from './login-layout.routing';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LoginLayoutRoutes),
        FormsModule,
        MatButtonModule,
        MatRippleModule,
        MatInputModule,
        MatTooltipModule
    ],
    declarations: [LoginComponent]
})
export class LoginLayoutModule {
}
