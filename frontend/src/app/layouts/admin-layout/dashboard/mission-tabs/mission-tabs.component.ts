import {Component, Input, OnInit} from '@angular/core';
import {MissionService} from '../../mission/service/mission.service';
import {Mission} from '../../mission/model/mission';
import {MissionStats} from '../model/mission.stats';
import * as Chartist from 'chartist';

@Component({
    selector: 'app-mission-tabs',
    templateUrl: './mission-tabs.component.html',
    styleUrls: ['./mission-tabs.component.scss']
})
export class MissionTabsComponent implements OnInit {

    @Input()
    mission: Mission;
    missionStats: MissionStats = new MissionStats();

    constructor(private missionService: MissionService) {
    }

    ngOnInit() {
        this.missionService.refreshMissionStats(this.mission.id, 1).subscribe(data => {
            this.missionStats = data;
            this.initBarChart();
        });
    }

    initBarChart() {
        const baseContext = this;
        const datawebsiteViewsChart = {
            labels: [],
            series: [
                []
            ]
        };
        let maxWeight = 0
        this.missionStats.categories.forEach(function (category) {
            datawebsiteViewsChart.labels.push(category.name);
            let value = 0;
            baseContext.missionStats.stats.forEach(function (item) {
                if (item.item.category.id === category.id) {
                    value += item.removedWeight;
                }
            });
            if (maxWeight < value) {
                maxWeight = value;
            }
            datawebsiteViewsChart.series[0].push(value);
        });
        const optionswebsiteViewsChart = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: maxWeight + 1,
            chartPadding: {top: 0, right: 5, bottom: 0, left: 0}
        };
        const responsiveOptions: any[] = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];
        const websiteViewsChart = new Chartist.Bar('#websiteViewsChart', datawebsiteViewsChart, optionswebsiteViewsChart, responsiveOptions);

        // start animation for the Emails Subscription Chart
        this.startAnimationForBarChart(websiteViewsChart);
    }

    startAnimationForBarChart(chart) {
        let seq2: any, delays2: any, durations2: any;

        seq2 = 0;
        delays2 = 80;
        durations2 = 500;
        chart.on('draw', function (data) {
            if (data.type === 'bar') {
                seq2++;
                data.element.animate({
                    opacity: {
                        begin: seq2 * delays2,
                        dur: durations2,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });

        seq2 = 0;
    };


}
