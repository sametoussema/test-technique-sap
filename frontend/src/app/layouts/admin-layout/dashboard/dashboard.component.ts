import {Component, OnInit} from '@angular/core';
import * as Chartist from 'chartist';
import {MissionService} from '../mission/service/mission.service';
import {Mission} from '../mission/model/mission';
import {Category} from '../category/model/category';
import {Item} from '../item/model/item';
import {CategoryService} from '../category/service/category.service';
import {ItemService} from '../item/service/item.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    missions: Array<Mission> = [];
    categories: Array<Category> = [];
    items: Array<Item> = [];

    constructor(private missionService: MissionService,
                private itemService: ItemService,
                private categoryService: CategoryService) {
    }


    ngOnInit() {
        this.fetchAllMissions();
        this.fetchAllCategories();
        this.fetchAllItems();
    }

    fetchAllMissions() {
        this.missionService.getAllMissions().subscribe(data => {
            this.missions = data.content;
        });
    }

    fetchAllCategories() {
        this.categoryService.getAllCategorys().subscribe(data => {
            this.categories = data.content;
        })
    }

    fetchAllItems() {
        this.itemService.getAllItems().subscribe(data => {
            this.items = data.content;
        })
    }


}
