import {Mission} from '../../mission/model/mission';
import {Item} from '../../item/model/item';
import {Category} from '../../category/model/category';

export class MissionStats {
    mission: Mission = new Mission();
    stats: Array<ItemStats> = [];
    categories: Array<Category> = [];
}

export class ItemStats {
    item: Item = new Item();
    removedPercentage = 0;
    removedWeight = 0;
}
