import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../shared/service/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

    constructor(public authService: AuthService, private router: Router) {
    }

    ngOnInit() {
    }

    logout() {
        this.router.navigateByUrl('/login');
        this.authService.logout();
    }
}
