import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {AdminLayoutRoutes} from './admin-layout.routing';
import {DashboardComponent} from './dashboard/dashboard.component';
import {UserProfileComponent} from './user-profile/user-profile.component';

import {MatButtonModule, MatInputModule, MatRippleModule, MatTooltipModule,} from '@angular/material';
import {MissionService} from './mission/service/mission.service';
import {ItemService} from './item/service/item.service';
import {CategoryService} from './category/service/category.service';
import { MissionTabsComponent } from './dashboard/mission-tabs/mission-tabs.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        MatButtonModule,
        MatRippleModule,
        MatInputModule,
        MatTooltipModule
    ],
    declarations: [
        DashboardComponent,
        UserProfileComponent,
        MissionTabsComponent
    ],
    providers: [MissionService, ItemService, CategoryService]
})

export class AdminLayoutModule {
}
