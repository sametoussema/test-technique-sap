import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../shared/service/notification.service';
import {ItemService} from '../service/item.service';
import {Item} from '../model/item';
import {Category} from '../../category/model/category';
import {CategoryService} from '../../category/service/category.service';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
    isEditAction: boolean;
    itemId: string;
    item: Item = new Item();
    categories: Array<Category> = [];

    constructor(private itemService: ItemService,
                private router: Router,
                private categoryService: CategoryService,
                private notificationService: NotificationService,
                private route: ActivatedRoute) {
        this.isEditAction = this.router.url.indexOf('edit') !== -1;
    }

    ngOnInit() {
        if (this.isEditAction) {
            this.itemId = this.route.snapshot.paramMap.get('itemId');
            this.itemService.getItemById(this.itemId).subscribe(data => {
                this.item = data;
            })
        }
        this.getAllCategories();
    }

    getAllCategories() {
        this.categoryService.getAllCategorys().subscribe(data => {
            this.categories = data.content;
            this.item.category = this.categories[0];
        })
    }

    submit() {
        if (this.isEditAction) {
            this.itemService.updateItem(this.item).subscribe(data => {
                this.notificationService.showNotification('success', '', 'Item <b>' + this.item.description + '</b> has been edited');
                this.router.navigate(['/item'], {queryParams: {reload: true}});
            })
        } else {
            this.itemService.createItem(this.item).subscribe(data => {
                this.notificationService.showNotification('success', '', 'Item <b>' + this.item.description + '</b> has been added');
                this.router.navigate(['/item'], {queryParams: {reload: true}});
            });
        }
    }

}
