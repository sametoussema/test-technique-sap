import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from './list/list.component';
import {EditComponent} from './edit/edit.component';
import {ItemService} from './service/item.service';
import {FormsModule} from '@angular/forms';
import {MatButtonModule, MatInputModule, MatRippleModule, MatSelectModule, MatTooltipModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import {ItemRoutes} from './item.routing';
import {CategoryService} from '../category/service/category.service';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ItemRoutes),
        FormsModule,
        MatButtonModule,
        MatRippleModule,
        MatInputModule,
        MatTooltipModule,
        MatSelectModule
    ],
    declarations: [ListComponent, EditComponent],
    providers: []
})
export class ItemModule {
}
