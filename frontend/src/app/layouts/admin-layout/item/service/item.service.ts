import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '../../../../shared/service/generic.service';
import {Observable} from 'rxjs/Observable';
import {Config} from '../../../../shared/config/config';
import {Page} from '../../../../shared/model/page';
import {Item} from '../model/item';

@Injectable()
export class ItemService extends GenericService {

    static ITEM_ENDPOINT = '/item';

    constructor(private http: HttpClient) {
        super()
    }

    getAllItems(): Observable<Page<Item>> {
        return this.http.get(Config.baseUrl + ItemService.ITEM_ENDPOINT) as  Observable<Page<Item>>
    }

    getItemById(itemId: string): Observable<Item> {
        return this.http.get(Config.baseUrl + ItemService.ITEM_ENDPOINT + `/${itemId}`) as  Observable<Item>
    }

    createItem(item: Item): Observable<Item> {
        console.log(item)
        item.category = item.category.id;
        return this.http.post(Config.baseUrl + ItemService.ITEM_ENDPOINT, item) as  Observable<Item>
    }

    updateItem(item: Item): Observable<Item> {
        item.category = item.category.id;
        return this.http.put(Config.baseUrl + ItemService.ITEM_ENDPOINT + `/${item.id}`, item) as  Observable<Item>
    }

    deleteItem(item: Item) {
        return this.http.delete(Config.baseUrl + ItemService.ITEM_ENDPOINT + `/${item.id}`)
    }
}
