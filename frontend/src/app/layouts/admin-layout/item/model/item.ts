import {Category} from '../../category/model/category';

export class Item {
    id: string;
    description: string;
    weight: number;
    category: any = new Category();
}
