import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../../shared/service/notification.service';
import {Item} from '../model/item';
import {ItemService} from '../service/item.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    items: Array<Item> = [];

    constructor(private itemService: ItemService,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.fetchAllItems()
    }

    fetchAllItems() {
        this.itemService.getAllItems().subscribe(data => {
            this.items = data.content;
        })
    }

    delete(item: Item) {
        this.itemService.deleteItem(item).subscribe(data => {
            this.fetchAllItems();
            this.notificationService.showNotification('success', '', 'Item <b>' + item.description + '</b> has been deleted');
        })
    }

}
