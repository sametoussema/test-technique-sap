import {Component, OnInit} from '@angular/core';
import {Mission} from '../model/mission';
import {MissionService} from '../service/mission.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../shared/service/notification.service';
import {Item} from '../../item/model/item';
import {ItemService} from '../../item/service/item.service';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

    isEditAction: boolean;
    missionId: string;
    mission: Mission = new Mission();
    items: Array<Item> = [];
    itemsId: Array<string> = [];

    constructor(private missionService: MissionService,
                private router: Router,
                private itemService: ItemService,
                private notificationService: NotificationService,
                private route: ActivatedRoute) {
        this.isEditAction = this.router.url.indexOf('edit') !== -1;
    }

    ngOnInit() {
        this.fetchAllItems();
        if (this.isEditAction) {
            this.missionId = this.route.snapshot.paramMap.get('missionId');
            this.missionService.getMissionById(this.missionId).subscribe(data => {
                this.mission = data;
                this.setItemsIdOfMission();
            })
        }
    }

    fetchAllItems() {
        this.itemService.getAllItems().subscribe(data => {
            this.items = data.content;
        })
    }

    setItemsIdOfMission() {
        const itemsId = [];
        this.mission.items.forEach(function (item) {
            itemsId.push(item.id);
        });
        this.itemsId = itemsId;
    }

    submit() {
        this.mission.items = this.itemsId;
        if (this.isEditAction) {
            this.missionService.updateMission(this.mission).subscribe(data => {
                this.notificationService.showNotification('success', '', 'Mission <b>' + this.mission.name + '</b> has been edited');
                this.router.navigate(['/mission'], {queryParams: {reload: true}});
            })
        } else {
            this.missionService.createMission(this.mission).subscribe(data => {
                this.notificationService.showNotification('success', '', 'Mission <b>' + this.mission.name + '</b> has been added');
                this.router.navigate(['/mission'], {queryParams: {reload: true}});
            });
        }
    }

}
