export class Mission {
    id: string;
    name: string;
    maxWeight: number;
    items: Array<any> = [];
}
