import {Component, OnInit} from '@angular/core';
import {MissionService} from '../service/mission.service';
import {Mission} from '../model/mission';
import {NotificationService} from '../../../../shared/service/notification.service';
import {Item} from '../../item/model/item';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    missions: Array<Mission> = [];

    constructor(private missionService: MissionService,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.fetchAllMissions()
    }

    fetchAllMissions() {
        this.missionService.getAllMissions().subscribe(data => {
            this.missions = data.content;
        })
    }


    delete(mission: Mission) {
        this.missionService.deleteMission(mission).subscribe(data => {
            this.fetchAllMissions();
            this.notificationService.showNotification('success', '', 'Mission <b>' + mission.name + '</b> has been deleted');
        })
    }

    getItemsWeight(items: Array<Item>) {
        let sum = 0;
        items.forEach(function (item) {
            sum += item.weight;
        });
        return sum;
    }
}
