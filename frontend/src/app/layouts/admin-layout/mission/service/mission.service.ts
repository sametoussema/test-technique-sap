import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '../../../../shared/service/generic.service';
import {Observable} from 'rxjs/Observable';
import {Mission} from '../model/mission';
import {Config} from '../../../../shared/config/config';
import {Page} from '../../../../shared/model/page';
import {MissionStats} from '../../dashboard/model/mission.stats';

@Injectable()
export class MissionService extends GenericService {

    static MISSION_ENDPOINT = '/mission';

    constructor(private http: HttpClient) {
        super()
    }

    getAllMissions(): Observable<Page<Mission>> {
        return this.http.get(Config.baseUrl + MissionService.MISSION_ENDPOINT) as  Observable<Page<Mission>>
    }

    getMissionById(missionId: string): Observable<Mission> {
        return this.http.get(Config.baseUrl + MissionService.MISSION_ENDPOINT + `/${missionId}`) as  Observable<Mission>
    }

    createMission(mission: Mission): Observable<Mission> {
        return this.http.post(Config.baseUrl + MissionService.MISSION_ENDPOINT, mission) as  Observable<Mission>
    }

    updateMission(mission: Mission): Observable<Mission> {
        return this.http.put(Config.baseUrl + MissionService.MISSION_ENDPOINT + `/${mission.id}`, mission) as  Observable<Mission>
    }

    deleteMission(mission: Mission) {
        return this.http.delete(Config.baseUrl + MissionService.MISSION_ENDPOINT + `/${mission.id}`)
    }

    refreshMissionStats(missionId: string, ratio: number): Observable<MissionStats> {
        return this.http.post(Config.baseUrl + MissionService.MISSION_ENDPOINT + `/${missionId}/stats`, {
            ratio: ratio
        }) as  Observable<MissionStats>
    }
}
