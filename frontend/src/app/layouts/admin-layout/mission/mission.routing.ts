import {Routes} from '@angular/router';

import {EditComponent} from './edit/edit.component';
import {ListComponent} from './list/list.component';

export const MissionRoutes: Routes = [

    {path: '', component: ListComponent},
    {path: 'add', component: EditComponent},
    {path: ':missionId/edit', component: EditComponent}
];
