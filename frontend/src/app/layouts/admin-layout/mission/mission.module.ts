import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from './list/list.component';
import {EditComponent} from './edit/edit.component';
import {MissionService} from './service/mission.service';
import {RouterModule} from '@angular/router';
import {MissionRoutes} from './mission.routing';
import {FormsModule} from '@angular/forms';
import {MatButtonModule, MatInputModule, MatRippleModule, MatSelectModule, MatTooltipModule} from '@angular/material';
import {ItemService} from '../item/service/item.service';

@NgModule({
    imports: [
        RouterModule.forChild(MissionRoutes),
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatRippleModule,
        MatInputModule,
        MatTooltipModule,
        MatSelectModule
    ],
    declarations: [ListComponent, EditComponent],
    providers: []
})
export class MissionModule {
}
