import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from '../../../../shared/service/generic.service';
import {Observable} from 'rxjs/Observable';
import {Config} from '../../../../shared/config/config';
import {Page} from '../../../../shared/model/page';
import {Category} from '../model/Category';

@Injectable()
export class CategoryService extends GenericService {

    static CATEGORY_ENDPOINT = '/category';

    constructor(private http: HttpClient) {
        super()
    }

    getAllCategorys(): Observable<Page<Category>> {
        return this.http.get(Config.baseUrl + CategoryService.CATEGORY_ENDPOINT) as  Observable<Page<Category>>
    }

    getCategoryById(categoryId: string): Observable<Category> {
        return this.http.get(Config.baseUrl + CategoryService.CATEGORY_ENDPOINT + `/${categoryId}`) as  Observable<Category>
    }

    createCategory(category: Category): Observable<Category> {
        return this.http.post(Config.baseUrl + CategoryService.CATEGORY_ENDPOINT, category) as  Observable<Category>
    }

    updateCategory(category: Category): Observable<Category> {
        return this.http.put(Config.baseUrl + CategoryService.CATEGORY_ENDPOINT + `/${category.id}`, category) as  Observable<Category>
    }

    deleteCategory(category: Category) {
        return this.http.delete(Config.baseUrl + CategoryService.CATEGORY_ENDPOINT + `/${category.id}`)
    }
}
