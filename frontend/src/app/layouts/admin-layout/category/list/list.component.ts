import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../../shared/service/notification.service';
import {Category} from '../model/category';
import {CategoryService} from '../service/category.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
    categories: Array<Category> = [];

    constructor(private categoryService: CategoryService,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.fetchAllCategories()
    }

    fetchAllCategories() {
        this.categoryService.getAllCategorys().subscribe(data => {
            this.categories = data.content;
        })
    }

    delete(category: Category) {
        this.categoryService.deleteCategory(category).subscribe(data => {
            this.fetchAllCategories();
            this.notificationService.showNotification('success', '', 'Category <b>' + category.name + '</b> has been deleted');
        })
    }
}
