import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditComponent} from './edit/edit.component';
import {ListComponent} from './list/list.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {MatButtonModule, MatInputModule, MatRippleModule, MatTooltipModule} from '@angular/material';
import {CategoryRoutes} from './category.routing';
import {CategoryService} from './service/category.service';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CategoryRoutes),
        FormsModule,
        MatButtonModule,
        MatRippleModule,
        MatInputModule,
        MatTooltipModule
    ],
    declarations: [EditComponent, ListComponent],
    providers: []
})
export class CategoryModule {
}
