import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../shared/service/notification.service';
import {Category} from '../model/category';
import {CategoryService} from '../service/category.service';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
    isEditAction: boolean;
    categoryId: string;
    category: Category = new Category();

    constructor(private categoryService: CategoryService,
                private router: Router,
                private notificationService: NotificationService,
                private route: ActivatedRoute) {
        this.isEditAction = this.router.url.indexOf('edit') !== -1;
    }

    ngOnInit() {
        if (this.isEditAction) {
            this.categoryId = this.route.snapshot.paramMap.get('categoryId');
            this.categoryService.getCategoryById(this.categoryId).subscribe(data => {
                this.category = data;
            })
        }
    }

    submit() {
        if (this.isEditAction) {
            this.categoryService.updateCategory(this.category).subscribe(data => {
                this.notificationService.showNotification('success', '', 'Item <b>' + this.category.name + '</b> has been edited');
                this.router.navigate(['/category'], {queryParams: {reload: true}});
            })
        } else {
            this.categoryService.createCategory(this.category).subscribe(data => {
                this.notificationService.showNotification('success', '', 'Item <b>' + this.category.name + '</b> has been added');
                this.router.navigate(['/category'], {queryParams: {reload: true}});
            });
        }
    }
}
