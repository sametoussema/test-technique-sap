export class Category {
    id: string;
    name: string;
    priority: number;
}
