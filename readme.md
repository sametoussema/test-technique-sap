### Test Technique SAP

##Build dependencies
> cd backend

> ./gradlew clean build copyDockerFile

> cd ../frontend

> npm install (only first time)

> ng build


##Deploy on Docker

> docker network create -d bridge nginx-proxy (only first time)

> docker-compose up --build

Backend will be running on http://localhost:8080

Frontend will be running on http://localhost:4200

Admin Credentials: admin@sap.com | admin