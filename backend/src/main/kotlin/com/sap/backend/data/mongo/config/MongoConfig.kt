package com.sap.backend.data.mongo.config

import com.mongodb.client.gridfs.GridFSBucket
import com.mongodb.client.gridfs.GridFSBuckets
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.MongoTemplate


@Configuration
class MongoConfig {
    @Autowired
    lateinit var mongoTemplate: MongoTemplate

    @Bean
    fun getGridFSBuckets(): GridFSBucket {
        val db = mongoTemplate.db
        return GridFSBuckets.create(db)
    }
}