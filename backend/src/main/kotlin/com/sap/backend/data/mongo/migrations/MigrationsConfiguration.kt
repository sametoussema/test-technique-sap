package com.sap.backend.data.mongo.migrations

import com.github.mongobee.Mongobee
import com.mongodb.MongoClient
import com.sap.backend.data.mongo.config.MongoDBProperties
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.BeanInitializationException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.mongo.MongoProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.MongoTemplate

@ConditionalOnProperty("sap.libraries.mongodb.migrations.enabled")
@Configuration
class MigrationsConfiguration {

    val LOG: Logger = LoggerFactory.getLogger(MigrationsConfiguration::class.java)

    @Autowired
    lateinit var properties: MongoDBProperties

    @Bean
    fun mongobee(mongoClient: MongoClient, mongoTemplate: MongoTemplate, mongoProperties: MongoProperties): Mongobee {
        LOG.debug("Configuring Mongobee")

        if (properties.migrations.`package`.isEmpty())
            throw BeanInitializationException("Package description is empty. Please provide value in 'sap.libraries.mongodb.migrations.package'")

        val mongobee = Mongobee(mongoClient)
        mongobee.setDbName(mongoProperties.database ?: "test")
        mongobee.setMongoTemplate(mongoTemplate)
        // package to scan for migrations
        mongobee.setChangeLogsScanPackage(properties.migrations.`package`)
        mongobee.isEnabled = true
        return mongobee
    }
}
