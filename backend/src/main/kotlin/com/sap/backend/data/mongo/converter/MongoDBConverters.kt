package com.sap.backend.data.mongo.converter

import org.springframework.core.convert.converter.Converter

class MongoDBConverters: ArrayList<Converter<*, *>>()
