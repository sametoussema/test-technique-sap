package com.sap.backend.data.mongo.migrations.seeders

import com.github.mongobee.changeset.ChangeLog
import com.github.mongobee.changeset.ChangeSet
import com.sap.backend.api.category.model.Category
import com.sap.backend.api.item.model.Item
import com.sap.backend.api.mission.model.Mission
import com.sap.backend.api.user.model.User
import com.sap.backend.libraries.constants.Authority
import org.springframework.data.mongodb.core.MongoTemplate

@ChangeLog(order = "001")
class Initialization {

    @ChangeSet(order = "01", author = "initiator", id = "01-create-admin-user")
    fun intialize(mongoTemplate: MongoTemplate) {
        mongoTemplate.save(User(
                username = "admin@sap.com",
                password = "\$2a\$04\$0Zs/DErB4blyYIaalwR5U.1aN2nBsWfKwSV613l2njTZz4Kk8s.UW",// admin,
                active = true,
                authorities = mutableSetOf(Authority.ADMIN)
        ))
    }

    @ChangeSet(order = "02", author = "initiator", id = "02-create-mission-mars")
    fun intializeMission(mongoTemplate: MongoTemplate) {
        mongoTemplate.save(Mission(
                name = "Mission Mars",
                maxWeight = 28F
        ))
    }

    @ChangeSet(order = "03", author = "initiator", id = "03-create-category")
    fun intializeCategories(mongoTemplate: MongoTemplate) {
        mongoTemplate.save(Category(
                name = "Top Priority",
                priority = 1
        ))
        mongoTemplate.save(Category(
                name = "Less Top Priority",
                priority = 2
        ))
        mongoTemplate.save(Category(
                name = "Medium Priority",
                priority = 3
        ))
        mongoTemplate.save(Category(
                name = "Preferable",
                priority = 4
        ))
        mongoTemplate.save(Category(
                name = "Optional",
                priority = 5
        ))
    }

    @ChangeSet(order = "04", author = "initiator", id = "04-create-items")
    fun intializeItems(mongoTemplate: MongoTemplate) {
        val categories = mongoTemplate.findAll(Category::class.java)
        mongoTemplate.save(Item(
                description = "Item 0",
                weight = 14F,
                category = categories[0]
        ))
        mongoTemplate.save(Item(
                description = "Item 1",
                weight = 12F,
                category = categories[1]
        ))
        mongoTemplate.save(Item(
                description = "Item 2",
                weight = 8F,
                category = categories[2]
        ))
        mongoTemplate.save(Item(
                description = "Item 3",
                weight = 20F,
                category = categories[4]
        ))
    }

    @ChangeSet(order = "05", author = "initiator", id = "05-attach-mission-items")
    fun attachMissionWithItems(mongoTemplate: MongoTemplate) {
        val mission = mongoTemplate.findAll(Mission::class.java)
        mission[0].items = mongoTemplate.findAll(Item::class.java)
        mongoTemplate.save(mission[0])
    }
}
