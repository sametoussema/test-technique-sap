package com.sap.backend.libraries.security.data.service

import com.sap.backend.libraries.security.authentication.ITokenPersistence
import com.sap.backend.libraries.security.data.entity.PersistentToken
import com.sap.backend.libraries.security.data.repository.PersistentTokenRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.token.Token
import org.springframework.stereotype.Service

@Service
class PersistentTokenService : ITokenPersistence {

    val LOG: Logger = LoggerFactory.getLogger(PersistentTokenService::class.java)

    @Autowired
    lateinit var persistentTokenRepository: PersistentTokenRepository

    override fun exists(token: Token): Boolean {
        return persistentTokenRepository.findById(token.key).orElseGet { null } != null
    }

    override fun save(token: Token, principal: String): PersistentToken {
        return persistentTokenRepository.save(PersistentToken(token.key, principal))
    }

    override fun remove(token: Token): Boolean {
        persistentTokenRepository.deleteById(token.key)
        return true
    }

    fun removeAll(principal: String) {
        LOG.info("Cleaning tokens for $principal")
        persistentTokenRepository.deleteAll(persistentTokenRepository.findByPrincipal(principal))
    }
}
