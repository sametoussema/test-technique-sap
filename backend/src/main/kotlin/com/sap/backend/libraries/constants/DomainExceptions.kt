package com.sap.backend.libraries.constants

import com.sap.backend.libraries.errors.AbstractError
import org.springframework.http.HttpStatus
import java.io.Serializable

enum class DomainExceptions(var _message: String, val _httpStatus: HttpStatus = HttpStatus.INTERNAL_SERVER_ERROR) : AbstractError {

    FILE_NOT_FOUND("File not found", HttpStatus.NOT_FOUND),
    ACCOUNT_ALREADY_EXISTS("This account already exists.", HttpStatus.CONFLICT),
    ACCOUNT_UPDATE_FAILED("The request does not fulfill all security criteria.", HttpStatus.FORBIDDEN),
    ACCOUNT_WITHOUT_ID("Invalid account state.", HttpStatus.INTERNAL_SERVER_ERROR),


    USER_NOT_ACTIVATED("User not activated.", HttpStatus.FORBIDDEN),
    RESOURCE_NOT_FOUND("Resource Not found", HttpStatus.NOT_FOUND),
    RESOURCE_NOT_ALLOWED("Resource Not allowed", HttpStatus.BAD_REQUEST),

    GUEST_TOKEN_NOT_FOUND("Guest Token not found", HttpStatus.NOT_FOUND),
    JOB_ERROR("A job failed", HttpStatus.INTERNAL_SERVER_ERROR);

    override val statusCode: HttpStatus
        get() = this._httpStatus
    override val type: String
        get() = this.name
    override val message: String
        get() = this._message
    override val details: List<Serializable>
        get() = ArrayList()
    override val fields: List<Serializable>
        get() = ArrayList()
}
