package com.sap.backend.libraries.security

import org.springframework.security.config.annotation.web.builders.HttpSecurity

interface IHttpSecurityConfigurer {
    fun configure(http: HttpSecurity?)
}