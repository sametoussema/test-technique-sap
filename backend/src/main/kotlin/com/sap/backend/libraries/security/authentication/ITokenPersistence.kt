package com.sap.backend.libraries.security.authentication

import com.sap.backend.libraries.security.data.entity.PersistentToken
import org.springframework.security.core.token.Token

interface ITokenPersistence {

    fun exists(token: Token): Boolean
    fun save(token: Token, principal: String): PersistentToken
    fun remove(token: Token): Boolean

}
