package com.sap.backend.libraries.security.data.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority

interface IUser<T> {

    val authorities: Set<T>?

    val roles: List<GrantedAuthority>
        @JsonIgnore
        get() = authorities?.map { SimpleGrantedAuthority("ROLE_$it") }.orEmpty()

    fun getPrincipal(): String
    fun canAuthenticate(): Boolean
    fun getSecret(): String
}