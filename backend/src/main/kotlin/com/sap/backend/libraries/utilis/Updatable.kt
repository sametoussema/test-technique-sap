package com.sap.backend.libraries.utilis

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
annotation class Updatable(
        val nullable: Boolean = false,
        val inception: Boolean = false)
