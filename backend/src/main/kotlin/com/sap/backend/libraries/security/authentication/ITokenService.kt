package com.sap.backend.libraries.security.authentication

import org.springframework.security.core.Authentication
import org.springframework.security.core.token.Token
import org.springframework.security.core.token.TokenService
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

interface ITokenService : TokenService {
    fun addAuthentication(res: HttpServletResponse, username: String)
    fun getAuthentication(request: HttpServletRequest): Authentication?


    fun allocateAccessToken(authentication: Authentication): Token
    fun revokeToken(token: String)
}