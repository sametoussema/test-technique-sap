package com.sap.backend.libraries.security.jwt

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.token.Token

class JwtToken : Token {

    private val _secret: String?
    var claims: Claims = Jwts.claims()
        set(value) {
            field.putAll(value)
        }

    var token: String? = null
        set(value) {
            claims = Jwts.parser().setSigningKey(_secret).parseClaimsJws(value)?.body ?: return
            field = value
        }

    constructor(secret: String) {
        _secret = secret
    }

    fun getAuthorities(): List<GrantedAuthority> {
        return claims[JwtConstants.AUTHORITIES_KEY]?.toString()?.split(',')?.map { SimpleGrantedAuthority(it) }
                ?: return arrayListOf()
    }

    fun getSubject(): String {
        return claims.subject
    }

    fun isRefreshToken(): Boolean {
        return claims[JwtConstants.IS_REFRESH_KEY] != null
    }

    fun getAccessToken(): String? {
        return claims[JwtConstants.ACCESS_TOKEN_KEY]?.toString()
    }

    override fun getKey(): String {
        return token ?: ""
    }

    override fun getExtendedInformation(): String {
        return "${claims.subject};${claims[JwtConstants.AUTHORITIES_KEY].toString()}";
    }

    override fun getKeyCreationTime(): Long {
        return claims.expiration.time
    }
}