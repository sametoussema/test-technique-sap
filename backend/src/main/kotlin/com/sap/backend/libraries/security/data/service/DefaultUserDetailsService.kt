package com.sap.backend.libraries.security.data.service

import com.sap.backend.api.user.service.IUserService
import com.sap.backend.libraries.constants.DomainExceptions
import com.sap.backend.libraries.errors.APIException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import java.util.*

@ConditionalOnProperty("sap.libraries.security.enabled")
@Component
class DefaultUserDetailsService : UserDetailsService {

    private val log = LoggerFactory.getLogger(DefaultUserDetailsService::class.java)

    @Autowired
    lateinit var userService: IUserService

    override fun loadUserByUsername(login: String): UserDetails {
        val lowercaseEmail = login.toLowerCase(Locale.ENGLISH)
        val user = userService.findOneByPrincipal(lowercaseEmail)
                ?: throw UsernameNotFoundException("User ${lowercaseEmail} was not found in the database")
        if (!user.canAuthenticate()) {
            throw APIException(DomainExceptions.USER_NOT_ACTIVATED)
        }
        return User(lowercaseEmail, user.getSecret(), user.roles)
    }
}
