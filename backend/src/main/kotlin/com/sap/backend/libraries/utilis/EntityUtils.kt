package com.sap.backend.libraries.utilis

import org.apache.commons.lang3.reflect.FieldUtils
import org.springframework.beans.PropertyAccessorFactory

object EntityUtils {

    fun <V> update(entity: V, update: V, klass: Class<*>): V {
        val fields = FieldUtils.getFieldsListWithAnnotation(klass, Updatable::class.java)
        val entityAccessor = PropertyAccessorFactory.forDirectFieldAccess(entity as Any)
        val updateAccessor = PropertyAccessorFactory.forDirectFieldAccess(update as Any)

        for (field in fields) {
            val annotation = field.getAnnotation(Updatable::class.java)
            val updateValue = updateAccessor.getPropertyValue(field.name)
            val currentValue = entityAccessor.getPropertyValue(field.name)

            if (!annotation.nullable && updateValue == null)
                continue

            if (annotation.inception && currentValue != null && updateValue != null)
            // we need to go ... deeper ...
                entityAccessor.setPropertyValue(field.name, update(currentValue, updateValue, field.type))
            else
                entityAccessor.setPropertyValue(field.name, updateValue)
        }
        return entity
    }
}