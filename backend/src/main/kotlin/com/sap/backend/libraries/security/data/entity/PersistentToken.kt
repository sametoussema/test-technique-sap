package com.sap.backend.libraries.security.data.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "tokens")
data
class PersistentToken(
        @Id var token: String,
        @Indexed var principal: String
)
