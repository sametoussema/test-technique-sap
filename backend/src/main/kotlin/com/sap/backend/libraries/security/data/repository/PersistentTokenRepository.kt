package com.sap.backend.libraries.security.data.repository

import com.sap.backend.libraries.security.data.entity.PersistentToken
import org.springframework.data.mongodb.repository.MongoRepository

interface PersistentTokenRepository: MongoRepository<PersistentToken, String> {

    fun findByPrincipal(principal: String): List<PersistentToken>

}
