package com.sap.backend.libraries.security.authentication

import com.sap.backend.libraries.constants.Authority
import com.sap.backend.libraries.security.data.entity.IUser
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken

interface IAuthentificationService {
    class AuthenticationResult(var accessToken: String? = null, var user: IUser<Authority>? = null)

    fun authenticate(auth: UsernamePasswordAuthenticationToken): AuthenticationResult
    fun logout(accessTokenString: String, refreshTokenString: String)

}