package com.sap.backend.libraries.errors

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.validation.FieldError

class APIException : RuntimeException, AbstractError {

    override var type = "UNDEFINED_ERROR"
        private set
    override var statusCode = HttpStatus.INTERNAL_SERVER_ERROR
    override var details: MutableCollection<String> = ArrayList()
    override var fields: ArrayList<FieldErrorVM> = ArrayList()
    var headers: MutableList<HttpHeaders>? = null

    constructor(exception: AbstractError) : super(exception.message) {
        this.type = exception.type
        this.statusCode = exception.statusCode
    }

    constructor(message: String, exception: AbstractError) : super(message) {
        this.type = exception.type
        this.statusCode = exception.statusCode
    }

    constructor(details: Collection<String>, exception: AbstractError) : super(exception.message) {
        this.details = details.toMutableList()
        this.type = exception.type
        this.statusCode = exception.statusCode
    }

    constructor(type: String, message: String) : this(message, BaseExceptions.INTERNAL_SERVER_ERROR) {
        this.type = type
    }

    fun add(message: String): APIException {
        details.add(message)
        return this
    }

    fun field(fieldError: FieldError): APIException {
        return field(FieldErrorVM(fieldError.objectName, fieldError.field, fieldError.defaultMessage
                ?: "message not defined"))
    }

    fun field(fieldError: FieldErrorVM): APIException {
        fields.add(fieldError)
        return this
    }

    fun header(header: HttpHeaders): APIException {
        if (headers == null)
            headers = ArrayList()
        headers!!.add(header)
        return this
    }

    val fieldErrors: List<FieldErrorVM>?
        get() = this.fields
}

