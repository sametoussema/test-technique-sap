package com.sap.backend

import com.google.gson.GsonBuilder
import com.sap.backend.api.welcome.model.response.WelcomeResponse
import com.sap.backend.libraries.utilis.EnvironmentProfileUtils
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.net.InetAddress


@SpringBootApplication
class BackendApplication

fun main(args: Array<String>) {
    val app = SpringApplication(BackendApplication::class.java)
    EnvironmentProfileUtils.addDefaultProfile(app, "dev")
    val env = app.run(*args).environment
    val protocol = "http"
    val gson = GsonBuilder().setPrettyPrinting().create()

    println("----------------------------------------------------------\n" +
            gson.toJson(WelcomeResponse(env.getProperty("spring.application.description") ?: "APPLICQTION_NAME",
                    "green",
                    "$protocol://localhost:${env.getProperty("server.port") ?: 8080}",
                    "$protocol://${InetAddress.getLocalHost().hostAddress}:${env.getProperty("server.port")
                            ?: 8080}")
            ) +
            "\n----------------------------------------------------------\n\t")
}
