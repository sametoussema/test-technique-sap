package com.sap.backend.config

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Component
@ConfigurationProperties("application", ignoreUnknownFields = false)
class ApplicationProperties {

    val project = ProjectProperties()

    class ProjectProperties {

        @NotNull
        @Valid
        @NotBlank
        var contact: String = "contact@sap.com"

        @NotNull
        @Valid
        @NotBlank
        var cgus: String = "https://www.sap.com/cgus.html"

        /**
         * Hidden from /links results
         */

        @JsonIgnore
        @NotNull
        @Valid
        @NotBlank
        var from: String = contact

        @JsonIgnore
        @NotNull
        @Valid
        @NotBlank
        var adminEmail: String = from

        @JsonIgnore
        @NotNull
        @Valid
        @NotBlank
        var name: String = "SAPBackend"

        @JsonIgnore
        @NotNull
        @Valid
        @NotBlank
        var companyName: String = "SAP"

        @JsonIgnore
        @NotNull
        @Valid
        @NotBlank
        var baseUrl: String = "https://www.sap.com"

        @NotNull
        @Valid
        @JsonIgnore
        var resetPasswordExpiration: Long = (24 * 60 * 60)

    }
}
