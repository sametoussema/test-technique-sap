package com.sap.backend.config

import com.sap.backend.libraries.security.IHttpSecurityConfigurer
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.stereotype.Component


@Component
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfiguration : IHttpSecurityConfigurer {

    override fun configure(http: HttpSecurity?) {
        http ?: return

        http
                .authorizeRequests()
                .antMatchers("/api/user/guest/**").permitAll()
                .antMatchers("/api/**").permitAll()
    }
}
