package com.sap.backend.api.welcome.model.response

data class WelcomeResponse(
        val application: String,
        val status: String,
        val Local: String,
        val external: String
)