package com.sap.backend.api.category.model


import com.fasterxml.jackson.annotation.JsonIgnore
import com.sap.backend.libraries.utilis.Updatable
import org.springframework.data.annotation.*
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.time.LocalDateTime
import java.time.ZonedDateTime

@Document(collection = "category")
data class Category(

        @Updatable
        @Indexed(unique = true)
        var name: String,

        var priority: Int,

        @Id
        var id: String? = null,

        @JsonIgnore
        @Field("created_date")
        @CreatedDate
        var createdDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime(),

        @LastModifiedBy
        @Field("last_modified_by")
        @JsonIgnore
        var lastModifiedBy: String? = null,

        @LastModifiedDate
        @Field("last_modified_date")
        @JsonIgnore
        var lastModifiedDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime(),

        @Version
        @JsonIgnore
        val version: Long? = null

)
