package com.sap.backend.api.category.service

import com.sap.backend.api.category.model.Category
import com.sap.backend.api.category.service.repository.CategoryRepository
import com.sap.backend.libraries.constants.DomainExceptions
import com.sap.backend.libraries.errors.APIException
import com.sap.backend.libraries.utilis.EntityUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class CategoryService : ICategoryService {

    val LOG: Logger = LoggerFactory.getLogger(CategoryService::class.java)

    @Autowired
    lateinit var repository: CategoryRepository


    override fun create(Category: Category): Category {
        LOG.info("Creating $Category")
        return repository.save(Category)
    }

    override fun update(base: Category, update: Category?, save: Boolean): Category {
        LOG.info("Updating Category $base")

        val entity = if (update != null) {
            if (update.name.isNotBlank())
                base.name = update.name
            if (update.priority in 1..5) {
                base.priority = update.priority
            } else {
                throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
            }
            EntityUtils.update(base, update, Category::class.java)
        } else base

        return if (save)
            repository.save(entity)
        else base
    }

    override fun findAll(page: Pageable): Page<Category> {
        return repository.findAll(page)
    }

    override fun findOne(id: String): Category? {
        return repository.findById(id).get()
    }

    override fun findOneByName(name: String): Category? {
        return repository.findOneByName(name)
    }


    override fun delete(id: String) {
        repository.deleteById(id)
    }

    override fun addCategory(id: String, Category: Any): Category? {
        return findOne(id)
    }

    override fun refreshCategorysStatus(): List<Any> {
        return emptyList()
    }
}
