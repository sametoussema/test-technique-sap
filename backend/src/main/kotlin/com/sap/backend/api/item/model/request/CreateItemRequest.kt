package com.sap.backend.api.item.model.request

import com.sap.backend.api.item.model.Item
import org.jetbrains.annotations.NotNull
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Positive

class CreateItemRequest {

    @NotNull
    @NotBlank
    lateinit var description: String

    @NotNull
    @NotBlank
    @Positive
    var weight: Float = 0F


    @NotNull
    @NotBlank
    lateinit var category: String

    fun toItem() = Item(
            this.description,
            this.weight)
}