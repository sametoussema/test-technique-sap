package com.sap.backend.api.item.service

import com.sap.backend.api.item.model.Item
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface IItemService {
    fun create(Item: Item): Item
    fun update(base: Item, update: Item? = null, save: Boolean = true): Item
    fun findAll(page: Pageable): Page<Item>
    fun findOne(id: String): Item?
    fun delete(id: String)
    fun addItem(id: String, item: Any): Item?
    fun refreshItemsStatus(): List<Any>
}
