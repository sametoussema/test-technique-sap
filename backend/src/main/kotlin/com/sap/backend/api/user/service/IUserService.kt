package com.sap.backend.api.user.service

import com.sap.backend.api.user.model.User

interface IUserService {
    fun getConnectedUser(): User?
    fun findOneByUsername(username: String): User?
    fun findOneByPrincipal(principal: String): User?
}
