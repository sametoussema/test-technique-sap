package com.sap.backend.api.mission.service

import com.sap.backend.api.item.model.Item
import com.sap.backend.api.item.model.ItemStat
import com.sap.backend.api.mission.model.Mission
import com.sap.backend.api.mission.service.repository.MissionRepository
import com.sap.backend.libraries.utilis.EntityUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class MissionService : IMissionService {

    val LOG: Logger = LoggerFactory.getLogger(MissionService::class.java)

    @Autowired
    lateinit var repository: MissionRepository


    override fun create(mission: Mission): Mission {
        LOG.info("Creating $mission")
        return repository.save(mission)
    }

    override fun update(base: Mission, update: Mission?, save: Boolean): Mission {
        LOG.info("Updating Mission $base")

        val entity = if (update != null) {
            if (update.name.isNotBlank())
                base.name = update.name
            if (update.maxWeight > 0F)
                base.maxWeight = update.maxWeight
            EntityUtils.update(base, update, Mission::class.java)
        } else base

        return if (save)
            repository.save(entity)
        else base
    }

    override fun findAll(page: Pageable): Page<Mission> {
        return repository.findAll(page)
    }

    override fun findOne(id: String): Mission? {
        return repository.findById(id).get()
    }

    override fun findOneByName(name: String): Mission? {
        return repository.findOneByName(name)
    }


    override fun delete(id: String) {
        repository.deleteById(id)
    }

    override fun addItem(mission: Mission, newItem: Item): Mission? {
        val items = ArrayList<Item>()
        mission.items.forEach { items.add(it) }
        items.add(newItem)
        mission.items = items
        return repository.save(mission)
    }

    /**
     * This function is to refresh statistics which will break down the weight of all items per category to be loaded
     * for a given mission, it works like:
     *
     * - Calculate over weight
     * - Sort items by category priority
     * - Calculate the minimum part to be removed from items (the biggest priority has minim weight to be removed)
     *      => calculated like this: overWeight / sum(items.category.prioriy);
     *      so that the weight of the item with the biggest priority will be decreased by only one part,
     *      otherwise the weight of the item with the minim priority well be decreased by 5 parts
     * - Foreach items remove adequate weight
     *      => we will remove from each one (category.priority * part)
     * - to let this algo more dynamic, we added a variable named ration, the adequat weight to be removed will be:
     *      => we will remove from each one (category.priority * Math.pow(part,ration))
     *      so that the weight og the minim priority will be decreased more than those with biggest priority
     *
     */
    override fun refreshMissionStats(mission: Mission, ratio: Int): List<ItemStat> {
        var itemSumWeight = 0F
        val stats: ArrayList<ItemStat> = arrayListOf()
        mission.items.forEach {
            itemSumWeight += it.weight
            stats.add(ItemStat(it))
        }
        val overWeight = itemSumWeight - mission.maxWeight
        return if (overWeight > 0) {
            val items = mission.items.sortedBy { it.category?.priority }
            val categories = items.mapNotNull { it.category }
            var prioritySum = 0
            categories.forEach {
                prioritySum += Math.round(Math.pow(it.priority.toDouble(), ratio.toDouble())).toInt()
            }
            val removeWeightPart = overWeight / prioritySum
            var remainingWeight = 0F

            for (i in (items.size - 1) downTo 0) {
                val currentItem = items[i]
                val nbreOfRemovedParts = Math.round(Math.pow((currentItem.category?.priority
                        ?: 0).toDouble(), ratio.toDouble())).toInt()
                var removedWeight = ((removeWeightPart * nbreOfRemovedParts) + remainingWeight)
                if (removedWeight > currentItem.weight) {
                    remainingWeight = removedWeight - currentItem.weight
                    removedWeight = currentItem.weight
                } else {
                    remainingWeight = 0F
                }
                val removedPercentage = (removedWeight / currentItem.weight) * 100
                stats.filter { it.item.id == currentItem.id }.firstOrNull()?.apply {
                    this.removedWeight = removedWeight
                    this.removedPercentage = removedPercentage
                }
            }
            stats
        } else {
            emptyList()
        }
    }
}
