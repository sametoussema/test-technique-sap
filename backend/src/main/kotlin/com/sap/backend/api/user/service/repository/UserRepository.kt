package com.sap.backend.api.user.service.repository

import com.sap.backend.api.user.model.User
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRepository : MongoRepository<User, String> {

    fun findOneByUsername(username: String): User?

    fun findByIdIn(ids: List<String>): List<User>

}
