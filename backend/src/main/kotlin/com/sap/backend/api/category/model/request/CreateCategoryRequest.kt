package com.sap.backend.api.category.model.request

import com.sap.backend.api.category.model.Category
import org.jetbrains.annotations.NotNull
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Positive

class CreateCategoryRequest {

    @NotNull
    @NotBlank
    lateinit var name: String

    @Positive
    @NotBlank
    @NotNull
    var priority: Int = 1

    fun toCategory() = Category(
            this.name,
            this.priority)
}