package com.sap.backend.api.admin.role

import com.sap.backend.libraries.constants.Authority
import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Inherited
@PreAuthorize("hasRole('" + Authority.Constants.ADMIN + "')")
annotation class AdminEndpoint
