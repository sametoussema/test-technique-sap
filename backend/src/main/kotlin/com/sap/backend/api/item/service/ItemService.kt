package com.sap.backend.api.item.service

import com.sap.backend.api.item.model.Item
import com.sap.backend.api.item.service.repository.ItemRepository
import com.sap.backend.libraries.utilis.EntityUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class ItemService : IItemService {

    val LOG: Logger = LoggerFactory.getLogger(ItemService::class.java)

    @Autowired
    lateinit var repository: ItemRepository


    override fun create(Item: Item): Item {
        LOG.info("Creating $Item")
        return repository.save(Item)
    }

    override fun update(base: Item, update: Item?, save: Boolean): Item {
        LOG.info("Updating Item $base")

        val entity = if (update != null) {
            if (update.description.isNotBlank())
                base.description = update.description
            if (update.category != null)
                base.category = update.category
            EntityUtils.update(base, update, Item::class.java)
        } else base

        return if (save)
            repository.save(entity)
        else base
    }

    override fun findAll(page: Pageable): Page<Item> {
        return repository.findAll(page)
    }

    override fun findOne(id: String): Item? {
        return repository.findById(id).get()
    }


    override fun delete(id: String) {
        repository.deleteById(id)
    }

    override fun addItem(id: String, item: Any): Item? {
        return findOne(id)
    }

    override fun refreshItemsStatus(): List<Any> {
        return emptyList()
    }
}
