package com.sap.backend.api.item.service.repository

import com.sap.backend.api.item.model.Item
import com.sap.backend.api.mission.model.Mission
import org.springframework.data.mongodb.repository.MongoRepository

interface ItemRepository : MongoRepository<Item, String> {

}
