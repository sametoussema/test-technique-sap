package com.sap.backend.api.welcome.model.request

import org.jetbrains.annotations.NotNull

class WelcomeRequest {

    @NotNull
    lateinit var attr: String
}