package com.sap.backend.api.category.service

import com.sap.backend.api.category.model.Category
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface ICategoryService {
    fun create(Category: Category): Category
    fun update(base: Category, update: Category? = null, save: Boolean = true): Category
    fun findAll(page: Pageable): Page<Category>
    fun findOne(id: String): Category?
    fun findOneByName(name: String): Category?
    fun delete(id: String)
    fun addCategory(id: String, Category: Any): Category?
    fun refreshCategorysStatus(): List<Any>
}
