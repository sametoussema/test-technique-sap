package com.sap.backend.api.admin

import com.sap.backend.api.admin.role.AdminEndpoint
import com.sap.backend.api.manager.model.response.AdminResponse
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController


@RestController
@AdminEndpoint
@RequestMapping("/api/admin")
class AdminResource {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun get(page: Pageable): AdminResponse {
        return AdminResponse("Admin Resource")
    }

}