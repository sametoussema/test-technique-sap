package com.sap.backend.api.mission.service.repository

import com.sap.backend.api.mission.model.Mission
import org.springframework.data.mongodb.repository.MongoRepository

interface MissionRepository : MongoRepository<Mission, String> {

    fun findOneByName(name: String): Mission?

}
