package com.sap.backend.api.mission

import com.sap.backend.api.admin.role.AdminEndpoint
import com.sap.backend.api.item.service.IItemService
import com.sap.backend.api.mission.model.Mission
import com.sap.backend.api.mission.model.request.CreateMissionRequest
import com.sap.backend.api.mission.model.request.StatsMissionRequest
import com.sap.backend.api.mission.model.response.MissionStatsResponse
import com.sap.backend.api.mission.service.IMissionService
import com.sap.backend.libraries.constants.DomainExceptions
import com.sap.backend.libraries.errors.APIException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*


@RestController
@AdminEndpoint
@RequestMapping("/api/mission")
@CrossOrigin(origins = ["http://localhost:4200"])
class MissionResource {

    @Autowired
    lateinit var missionService: IMissionService

    @Autowired
    lateinit var itemService: IItemService

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun get(page: Pageable): Page<Mission> {
        return this.missionService.findAll(page)
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun get(@PathVariable("id") id: String): Mission {
        return this.missionService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.OK)
    fun create(@RequestBody createMissionRequest: CreateMissionRequest): Mission? {
        return this.missionService.create(createMissionRequest.toMission().apply {
            this.items = createMissionRequest.items.mapNotNull { itemService.findOne(it) }
        })
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun update(@PathVariable("id") id: String, @RequestBody createMissionRequest: CreateMissionRequest): Mission? {
        val mission = this.missionService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        return this.missionService.update(mission, createMissionRequest.toMission().apply {
            this.items = createMissionRequest.items.mapNotNull { itemService.findOne(it) }
        })
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun delete(@PathVariable("id") id: String) {
        this.missionService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        this.missionService.delete(id)
    }

    @PutMapping("{id}/item/{itemId}")
    @ResponseStatus(HttpStatus.OK)
    fun addItem(@PathVariable("id") id: String, @PathVariable("itemId") itemId: String,
                @RequestBody createMissionRequest: CreateMissionRequest): Mission? {
        val mission = this.missionService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        val item = this.itemService.findOne(itemId) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        return this.missionService.addItem(mission, item)
    }

    @PostMapping("{id}/stats")
    @ResponseStatus(HttpStatus.OK)
    fun stats(@PathVariable("id") id: String, @RequestBody() statsMissionRequest: StatsMissionRequest): MissionStatsResponse? {
        val mission = this.missionService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        val stats = this.missionService.refreshMissionStats(mission, statsMissionRequest.ratio)
        return MissionStatsResponse(mission, stats, mission.items.mapNotNull { it.category }.distinctBy { it.name }.sortedBy { it.priority })
    }


}