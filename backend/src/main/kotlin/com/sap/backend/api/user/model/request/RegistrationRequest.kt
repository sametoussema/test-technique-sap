package com.sap.backend.api.user.model.request

import com.sap.backend.api.user.model.User
import com.sap.backend.api.user.model.request.validator.AuthoritiesRequestValidator
import com.sap.backend.libraries.constants.Authority
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull

class RegistrationRequest {

    @Email
    @NotNull
    lateinit var username: String

    @NotNull
    lateinit var password: String

    @NotNull
    @AuthoritiesRequestValidator
    var authorities: MutableSet<Authority>? = mutableSetOf()

    fun toUser() = User(
            username.trim().toLowerCase(),
            password,
            authorities = authorities)
}