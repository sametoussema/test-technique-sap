package com.sap.backend.api.mission.model.request

import com.sap.backend.api.mission.model.Mission
import org.jetbrains.annotations.NotNull
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive

class CreateMissionRequest {

    @NotNull
    lateinit var name: String

    @NotNull
    @NotEmpty
    @Positive
    var maxWeight: Float = 0F

    @NotNull
    @NotEmpty
    lateinit var items: List<String>

    fun toMission() = Mission(
            this.name,
            this.maxWeight)
}