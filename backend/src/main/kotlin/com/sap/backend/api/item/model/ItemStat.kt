package com.sap.backend.api.item.model

data class ItemStat(
        val item: Item,
        var removedPercentage: Float = 0f,
        var removedWeight: Float = 0f
)