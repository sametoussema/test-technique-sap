package com.sap.backend.api.item

import com.sap.backend.api.admin.role.AdminEndpoint
import com.sap.backend.api.category.service.ICategoryService
import com.sap.backend.api.item.model.Item
import com.sap.backend.api.item.model.request.CreateItemRequest
import com.sap.backend.api.item.service.ItemService
import com.sap.backend.libraries.constants.DomainExceptions
import com.sap.backend.libraries.errors.APIException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*


@RestController
@AdminEndpoint
@RequestMapping("/api/item")
@CrossOrigin(origins = ["http://localhost:4200"])
class ItemResource {

    @Autowired
    lateinit var itemService: ItemService

    @Autowired
    lateinit var categoryService: ICategoryService

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun get(page: Pageable): Page<Item> {
        return this.itemService.findAll(page)
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun get(@PathVariable("id") id: String): Item {
        return this.itemService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.OK)
    fun create(@RequestBody createItemRequest: CreateItemRequest): Item? {
        val category = createItemRequest.category.let {
            categoryService.findOne(it) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        }
        return this.itemService.create(createItemRequest.toItem().apply { this.category = category })
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun update(@PathVariable("id") id: String, @RequestBody createItemRequest: CreateItemRequest): Item? {
        val Item = this.itemService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        val category = createItemRequest.category.let {
            categoryService.findOne(it) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        }
        return this.itemService.update(Item, createItemRequest.toItem().apply { this.category = category })
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun delete(@PathVariable("id") id: String) {
        this.itemService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        this.itemService.delete(id)
    }

}