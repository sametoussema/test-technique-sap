package com.sap.backend.api.mission.model.response

import com.sap.backend.api.category.model.Category
import com.sap.backend.api.item.model.ItemStat
import com.sap.backend.api.mission.model.Mission

data class MissionStatsResponse(
        val mission: Mission,
        val stats: List<ItemStat>,
        val categories: List<Category>
)