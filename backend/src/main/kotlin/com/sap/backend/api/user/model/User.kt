package com.sap.backend.api.user.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.sap.backend.libraries.constants.Authority
import com.sap.backend.libraries.constants.DomainExceptions
import com.sap.backend.libraries.errors.APIException
import com.sap.backend.libraries.security.data.entity.IUser
import com.sap.backend.libraries.utilis.Updatable
import org.springframework.data.annotation.*
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.time.LocalDateTime
import java.time.ZonedDateTime

@Document(collection = "users")
data class User(

        @Updatable
        @Indexed(unique = true)
        var username: String,

        @Updatable
        @JsonIgnore
        var password: String,

        var active: Boolean? = true,

        override var authorities: MutableSet<Authority>? = mutableSetOf(),

        @Id
        var id: String? = null,


        @CreatedBy
        @Field("created_by")
        @JsonIgnore
        var createdBy: String? = null,

        @JsonIgnore
        @Field("created_date")
        @CreatedDate
        var createdDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime(),

        @LastModifiedBy
        @Field("last_modified_by")
        @JsonIgnore
        var lastModifiedBy: String? = null,

        @LastModifiedDate
        @Field("last_modified_date")
        @JsonIgnore
        var lastModifiedDate: LocalDateTime = ZonedDateTime.now().toLocalDateTime(),

        @Version
        @JsonIgnore
        val version: Long? = null

) : IUser<Authority> {

    @JsonIgnore
    override fun getPrincipal(): String = id ?: throw APIException(DomainExceptions.ACCOUNT_WITHOUT_ID)

    @JsonIgnore
    override fun canAuthenticate(): Boolean = this.active ?: false

    @JsonIgnore
    override fun getSecret(): String = password

}
