package com.sap.backend.api.user

import com.sap.backend.api.user.model.User
import com.sap.backend.api.user.model.request.LoginRequest
import com.sap.backend.api.user.model.request.RegistrationRequest
import com.sap.backend.api.user.service.UserService
import com.sap.backend.libraries.constants.DomainExceptions
import com.sap.backend.libraries.errors.APIException
import com.sap.backend.libraries.security.authentication.IAuthentificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping("/api/user")
class UserResource {


    @Autowired
    lateinit var userService: UserService
    @Autowired
    lateinit var jwtAuthentificationService: IAuthentificationService


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun get(page: Pageable): Page<User> {
        return userService.find(page)
    }

    @GetMapping("me")
    @ResponseStatus(HttpStatus.OK)
    fun me(): User {
        return userService.getConnectedUserOrThrow()
    }

    @PostMapping("guest/register")
    @ResponseStatus(HttpStatus.CREATED)
    fun registerCustomer(@Valid @RequestBody creation: RegistrationRequest): User {

        val request = creation.toUser()

        if (!userService.isUsernameAvailable(request.username))
            throw APIException(DomainExceptions.ACCOUNT_ALREADY_EXISTS)

        val account = userService.create(request)
        return userService.update(account)
    }


    @PostMapping("/guest/login")
    @ResponseStatus(HttpStatus.OK)
    fun authenticate(@Valid @RequestBody login: LoginRequest): IAuthentificationService.AuthenticationResult {
        val authenticationToken = UsernamePasswordAuthenticationToken(login.username, login.password)
        return jwtAuthentificationService.authenticate(authenticationToken).let {
            it.user = userService.getConnectedUser()
            it
        }
    }
}