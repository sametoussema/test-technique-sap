package com.sap.backend.api.welcome.model.response

data class UserLoginResponse(val message: String)