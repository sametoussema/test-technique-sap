package com.sap.backend.api.category

import com.sap.backend.api.admin.role.AdminEndpoint
import com.sap.backend.api.category.model.Category
import com.sap.backend.api.category.model.request.CreateCategoryRequest
import com.sap.backend.api.category.service.CategoryService
import com.sap.backend.libraries.constants.DomainExceptions
import com.sap.backend.libraries.errors.APIException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*


@RestController
@AdminEndpoint
@RequestMapping("/api/category")
@CrossOrigin(origins = ["http://localhost:4200"])
class CategoryResource {

    @Autowired
    lateinit var categoryService: CategoryService

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun get(page: Pageable): Page<Category> {
        return this.categoryService.findAll(page)
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun get(@PathVariable("id") id: String): Category {
        return this.categoryService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.OK)
    fun create(@RequestBody createCategoryRequest: CreateCategoryRequest): Category? {
        return this.categoryService.create(createCategoryRequest.toCategory())
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun update(@PathVariable("id") id: String, @RequestBody createCategoryRequest: CreateCategoryRequest): Category? {
        val Category = this.categoryService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        return this.categoryService.update(Category, createCategoryRequest.toCategory())
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    fun delete(@PathVariable("id") id: String) {
        this.categoryService.findOne(id) ?: throw APIException(DomainExceptions.RESOURCE_NOT_FOUND)
        this.categoryService.delete(id)
    }

}