package com.sap.backend.api.mission.model.request

import org.jetbrains.annotations.NotNull
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive

class StatsMissionRequest {


    @NotNull
    @NotEmpty
    @Positive
    var ratio: Int = 0
}