package com.sap.backend.api.category.service.repository

import com.sap.backend.api.category.model.Category
import org.springframework.data.mongodb.repository.MongoRepository

interface CategoryRepository : MongoRepository<Category, String> {

    fun findOneByName(name: String): Category?

}
