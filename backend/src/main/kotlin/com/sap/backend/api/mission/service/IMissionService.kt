package com.sap.backend.api.mission.service

import com.sap.backend.api.item.model.Item
import com.sap.backend.api.item.model.ItemStat
import com.sap.backend.api.mission.model.Mission
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface IMissionService {
    fun create(mission: Mission): Mission
    fun update(base: Mission, update: Mission? = null, save: Boolean = true): Mission
    fun findAll(page: Pageable): Page<Mission>
    fun findOne(id: String): Mission?
    fun findOneByName(name: String): Mission?
    fun delete(id: String)
    fun addItem(mission: Mission, newItem: Item): Mission?
    fun refreshMissionStats(mission: Mission, ratio: Int): List<ItemStat>
}
