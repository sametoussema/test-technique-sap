package com.sap.backend.api.manager.model.response

data class AdminResponse(
        val message: String
)